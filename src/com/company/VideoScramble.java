package com.company;


import java.util.Random;

public class VideoScramble {

    Random r = new Random(System.currentTimeMillis());
    Random r2 = new Random(System.currentTimeMillis());
    Random r3 = new Random(System.currentTimeMillis());
    Random r4 = new Random(System.currentTimeMillis());
    String[] moves = {"Rx", "Lx", "Uy", "Dy", "Fz", "Bz"};
    String[] direccion = {" ", "' ", "2 "};
    String[] moves2 = {"Rx", "Uy", "Fz"};
    String[] direccion2 = {" ", "' ", "2 "};
    String[] moves3 = {"Rx", "Lx", "Uy", "Dy", "Fz", "Bz"};
    String[] direccion3 = {" ", "' ", "2 ", "w ", "w2 ", "w' "};
    String[] moves4 = {"Rx", "Lx", "Uy", "Dy", "Fz", "Bz"};
    String[] direccion4 = {" ", "' ", "2 ", "w ", "w2 ", "w' "};

    public static void main(String... rubiks) {
        VideoScramble cube = new VideoScramble();
        cube.getScramble();
    }

    public void getScramble() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 25; i++) {
            String currentMove = getMove2(move1, move2);
            direccio = direccion[r.nextInt(direccion.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        System.out.println(scramble);


    }

    public void getScramble2x2() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 10; i++) {
            String currentMove = getMove2(move1, move2);
            direccio = direccion2[r2.nextInt(direccion2.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        System.out.println(scramble);


    }
    public void delaySegundo() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }

    }
    public void delaySegundo2() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }

    }

    public void getScramble4x4() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 47; i++) {
            String currentMove = getMove3(move1, move2);
            direccio = direccion3[r3.nextInt(direccion3.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        System.out.println(scramble);


    }

    public void getScramble5x5() {
        String scramble = "";
        String move1 = "  ";
        String move2 = "  ";
        String direccio = "";

        for (int i = 0; i < 60; i++) {
            String currentMove = getMove3(move1, move2);
            direccio = direccion4[r4.nextInt(direccion4.length)];
            scramble += currentMove.charAt(0) + direccio;
            move1 = move2;
            move2 = currentMove;
        }
        System.out.println(scramble);


    }

    public String getMove(String m1, String m2) {
        String move = moves[r.nextInt(moves.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove(m1, m2);

        }

        return move;
    }

    public String getMove2(String m1, String m2) {
        String move = moves2[r2.nextInt(moves2.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove2(m1, m2);

        }

        return move;
    }

    public String getMove3(String m1, String m2) {
        String move = moves3[r3.nextInt(moves3.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove3(m1, m2);

        }

        return move;
    }

    public String getMove4(String m1, String m2) {
        String move = moves4[r4.nextInt(moves4.length)];
        if (m2 == move || repetit(m1, m2, move) == true) {
            return getMove4(m1, m2);

        }

        return move;
    }

    public boolean repetit(String m1, String m2, String m3) {
        if (m2.charAt(1) == m1.charAt(1) && m2.charAt(1) == m3.charAt(1)) {
            return true;

        }
        return false;
    }
}
